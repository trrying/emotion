# 表情平滑切换工具类 #
![Support](https://img.shields.io/badge/support-Android4.0+-blue.svg?style=flat)

##

![Demo](/image/01.gif)


上面是用了EmotionInputDetector类来控制输入法和操作面板之间的切换。与微信聊天界面类似的。

##Usage
在Activity中：

```
#!java
        //一个按钮对应一个操作面板
        Map<View, View> mActionViews = new HashMap<View, View>();
        mActionViews.put(iv_speak, rl_speaking);
        mActionViews.put(iv_image, rl_images);
        mActionViews.put(iv_emotion, rl_emoji);

        mDetector = EmotionInputDetector.with(this)
                .bindToContent(lv_content)
                .bindToEditText(ed_editText)
                .bindToEmotionButton(mActionViews)
                .build();
```

在xml布局中，要保证根布局为LinearLayout；

##原理
###将布局分为上下两段
- 上段为内容展示之类的，将其layout_height设为0，将layout_weight设为1；使其高度占满剩余高度；
- 下段为按钮和操作面板，面板显示时和输入法高度一致

###当点击按钮显示面板时，判断当前输入发是否已经弹出，如果弹出则
- 锁定显示内容lv_content的高度
- 隐藏输入法
- 设置操作面板高度为输入法高度
这样操作面板和输入法之间的切换就不会造成闪动。

##Thanks
dss886