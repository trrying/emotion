package com.owm.emotion;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.owm.emotion.utils.EmotionInputDetector;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {

    private ListView lv_content;

    private EditText ed_editText;

    private ImageView iv_speak;
    private ImageView iv_image;
    private ImageView iv_emotion;

    private RelativeLayout rl_speaking;
    private RelativeLayout rl_images;
    private RelativeLayout rl_emoji;

    private EmotionInputDetector mDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();


    }

    private void init() {
        lv_content = (ListView) findViewById(R.id.lv_content);

        ed_editText = (EditText) findViewById(R.id.ed_editText);

        iv_speak = (ImageView) findViewById(R.id.iv_speak);
        iv_image = (ImageView) findViewById(R.id.iv_image);
        iv_emotion = (ImageView) findViewById(R.id.iv_emotion);


        rl_speaking = (RelativeLayout) findViewById(R.id.rl_speaking);
        rl_images = (RelativeLayout) findViewById(R.id.rl_images);
        rl_emoji = (RelativeLayout) findViewById(R.id.rl_emoji);


        Map<View, View> mActionViews = new HashMap<View, View>();
        mActionViews.put(iv_speak, rl_speaking);
        mActionViews.put(iv_image, rl_images);
        mActionViews.put(iv_emotion, rl_emoji);

        mDetector = EmotionInputDetector.with(this)
                .bindToContent(lv_content)
                .bindToEditText(ed_editText)
                .bindToEmotionButton(mActionViews)
                .build();


    }




}
