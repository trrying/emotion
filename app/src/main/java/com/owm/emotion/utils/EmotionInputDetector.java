package com.owm.emotion.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 底部表情块和输入法之前切换工具类，避免界面跳动
 * Created by dss886 on 15/9/26.
 */
public class EmotionInputDetector implements OnClickListener {

    /**
     * SharedPreferences 文件名
     */
    private static final String SHARE_PREFERENCE_NAME = "com.dss886.emotioninputdetector";
    /**
     * 输入法高度
     */
    private static final String SHARE_PREFERENCE_TAG = "soft_input_height";

    private Activity mActivity;
    private InputMethodManager mInputManager;
    private SharedPreferences sp;
//    private View mEmotionLayout;
    private View mEditText;
    /**
     * 显示内容
     */
    private View mContentView;

    /**
     * 界面中有多个输入框，将输入框集合放在这里，便于控制输入法和操作界面切换
     */
    private List<View> mEditTexts;

    /**
     * 界面中有多个操作布局，将激活按钮和对应布局放在map中
     */
    private Map<View, View> mActionView = new HashMap<View, View>();

    private List<OnLayoutChangeListener> onLayoutChangeListenerList = new ArrayList<OnLayoutChangeListener>();

    private EmotionInputDetector() {}

    /**
     * 获取实例
     * @param activity
     * @return
     */
    public static EmotionInputDetector with(Activity activity) {
        EmotionInputDetector emotionInputDetector = new EmotionInputDetector();
        emotionInputDetector.mActivity = activity;
        emotionInputDetector.mInputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        emotionInputDetector.sp = activity.getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return emotionInputDetector;
    }

    /**
     * 绑定内容
     * @param contentView
     * @return
     */
    public EmotionInputDetector bindToContent(View contentView) {
        mContentView = contentView;
        return this;
    }

    public EmotionInputDetector bindToOtherEditText(List<View> editTexts){
        mEditTexts = editTexts;
        if (mEditTexts != null && !mEditTexts.isEmpty()){
            for (int i = 0; i < mEditTexts.size(); i++) {
                mEditTexts.get(i).setOnTouchListener(onTouchListener);
            }
        }
        return this;
    }

    /**
     * 用户点击输入框，如果有操作布局显示，则隐藏操作布局，显示输入法；否则不处理，由系统弹出输入法
     */
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP && isLayoutShow()) {
//                LogUtils.info("按下其他输入框并且布局显示，隐藏布局");
                lockContentHeight();
                hideEmotionLayout(false);
                setAllUnSelected(null);
                mContentView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        unlockContentHeightDelayed();
                    }
                }, 200L);
            }
            return false;
        }
    };

    /**
     * 绑定输入框
     * @param editText
     * @return
     */
    public EmotionInputDetector bindToEditText(View editText) {
        mEditText = editText;
        mEditText.requestFocus();
        mEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //触摸输入框，如果当前有布局显示，则锁定内容显示高度，然后隐藏布局并显示布局；定时200毫秒后解除内容布局锁定
                if (event.getAction() == MotionEvent.ACTION_UP && isLayoutShow()) {
                    lockContentHeight();
                    hideEmotionLayout(true);

                    mEditText.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            unlockContentHeightDelayed();
                        }
                    }, 200L);
                }
                return false;
            }
        });
        return this;
    }

    public EmotionInputDetector bindToEmotionButton(Map<View, View> actionView) {
        mActionView.clear();
        mActionView.putAll(actionView);
        for (View key : actionView.keySet()){
            key.setOnClickListener(this);
        }
        return this;
    }

    /**
     * 设置输入法弹出方式，将布局顶上去
     * @return
     */
    public EmotionInputDetector build(){
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        hideSoftInput();
        return this;
    }

    /**
     * 处理返回操作隐藏表情框或者输入法
     * @return
     */
    public boolean interceptBackPress() {
        if (isLayoutShow()) {
            hideEmotionLayout(false);
            return true;
        }
        return false;
    }

    /**
     * 隐藏输入法,展示布局
     */
    private void showEmotionLayout(View layout) {
//        LogUtils.info("layout-->"+layout);
        if (layout == null){
            return;
        }
        int softInputHeight = getSupportSoftInputHeight();
        if (softInputHeight == 0) {
            softInputHeight = sp.getInt(SHARE_PREFERENCE_TAG, 400);
        }
//        LogUtils.info("softInputHeight-->" + softInputHeight);
        hideSoftInput();
        hideAllLayout();
        layout.getLayoutParams().height = softInputHeight;
        layout.setVisibility(View.VISIBLE);
        onLayoutChange(layout);
    }

    /**
     * 隐藏所有布局
     */
    private void hideAllLayout(){
        for (View layout : mActionView.values()){
            layout.setVisibility(View.GONE);
        }
    }

    /**
     * 隐藏所有操作按钮，显示 actionView 按钮
     * @param actionView
     */
    private void setAllUnSelected(View actionView){
        for (View key : mActionView.keySet()){
            key.setSelected(false);
        }
        if (actionView != null) {
            actionView.setSelected(true);
        }
    }

    /**
     * 隐藏表情框，展示输入法
     * @param showSoftInput
     */
    private void hideEmotionLayout(boolean showSoftInput) {
        hideAllLayout();
        if (showSoftInput) {
            showSoftInput();
        }
        onLayoutChange(mEditText);
    }

    /**
     * 设置显示内容界面固定高度，防止高度扩张
     */
    private void lockContentHeight() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mContentView.getLayoutParams();
        params.height = mContentView.getHeight();
        params.weight = 0.0F;
    }

    /**
     * 取消显示内容界面固定高度，高度自适应
     */
    private void unlockContentHeightDelayed() {
        mEditText.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((LinearLayout.LayoutParams) mContentView.getLayoutParams()).weight = 1.0F;
            }
        }, 200L);
    }

    /**
     * 显示输入法
     */
    private void showSoftInput() {
        mEditText.requestFocus();
        setAllUnSelected(null);
        mEditText.post(new Runnable() {
            @Override
            public void run() {
                mInputManager.showSoftInput(mEditText, 0);
            }
        });
    }

    /**
     * 隐藏输入法
     */
    private void hideSoftInput() {
        mInputManager.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    /**
     * 输入法是否在显示，根据输入发高度判断
     * @return
     */
    private boolean isSoftInputShown() {
        return getSupportSoftInputHeight() != 0;
    }

    /**
     * 获取输入法高度
     * @return
     */
    public int getSupportSoftInputHeight() {
        Rect r = new Rect();
        mActivity.getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
        int screenHeight = mActivity.getWindow().getDecorView().getRootView().getHeight();
        int softInputHeight = screenHeight - r.bottom;
        Log.i("JIMIHUA", "r-->" + r.toString() + " screenHeight-->" + screenHeight + " softInputHeight-->" + softInputHeight);
//        if (Build.VERSION.SDK_INT >= 20) {
//            // When SDK Level >= 20 (Android L), the softInputHeight will contain the height of softButtonsBar (if has)
//            LogUtils.info(getClass().getName(),"softInputHeight-->"+softInputHeight+" getSoftButtonsBarHeight()-->"+getSoftButtonsBarHeight());
//            softInputHeight = softInputHeight - getSoftButtonsBarHeight();
//        }
        if (softInputHeight < 0) {
            Log.w("EmotionInputDetector", "Warning: value of softInputHeight is below zero!");
        }
        if (softInputHeight > 0) {
            sp.edit().putInt(SHARE_PREFERENCE_TAG, softInputHeight).apply();
        }
        return Math.max(softInputHeight, 0);
    }

    /**
     * 获取虚拟按键高度，
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private int getSoftButtonsBarHeight() {
        DisplayMetrics metrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int usableHeight = metrics.heightPixels;
        mActivity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int realHeight = metrics.heightPixels;
        if (realHeight > usableHeight) {
            return realHeight - usableHeight;
        } else {
            return 0;
        }
    }

    /**
     * 判断当前是否有布局在显示
     * @return
     */
    private boolean isLayoutShow(){
        boolean flag = false;
        for (View layout : mActionView.values()){
            if (layout.isShown()){
                return true;
            }
        }
        return flag;
    }

    @Override
    public void onClick(View v) {
        if (mActionView.get(v).getVisibility() == View.VISIBLE) {
//            LogUtils.info(" 当前layout可见，隐藏layout，显示输入法");
            lockContentHeight();//锁定高度
            hideEmotionLayout(true);//隐藏布局，显示输入法
            unlockContentHeightDelayed();//解锁高度
        } else {
            if (isSoftInputShown()) {
//                LogUtils.info(" 当前layout不可见，输入法可见，隐藏输入法，显示布局");
                lockContentHeight();//锁定内容高度，防止跳动
                setAllUnSelected(v);
                showEmotionLayout(mActionView.get(v));
                unlockContentHeightDelayed();//操作完成，解锁内容高度
            } else {
//                LogUtils.info(" 当前layout不可见，输入法不可见，直接显示布局");
                setAllUnSelected(v);
                showEmotionLayout(mActionView.get(v));
            }
        }
    }

    private void onLayoutChange(View view){
        for (int i = 0; i < onLayoutChangeListenerList.size(); i++) {
            onLayoutChangeListenerList.get(i).onLayoutChange(view);
        }
    }

    public void addOnlayoutChangeListener(OnLayoutChangeListener onLayoutChangeListener){
        this.onLayoutChangeListenerList.add(onLayoutChangeListener);
    }

    public interface OnLayoutChangeListener {
        public void onLayoutChange(View view);
    }
}
